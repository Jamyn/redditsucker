#!/bin/bash
apt-get install python-pip
pip install praw

echo "RedditSucker  Copyright (C) 2014 Iceberg Technologies Limited"
echo "This program comes with ABSOLUTELY NO WARRANTY"
echo " "
echo "Done installing, copy example-config.py to config.py and update to your liking."
echo " "
echo "Once you're done, Run the script manually with python redditsucker.py or set a cron job!"
