#The Reddit Sucker
The Reddit Sucker works by watching the specified subreddits. Whenever a new link is posted, it follows the link and downloads every file recursively. This allows for the ultimate in automated DataHoarding. This script is a spin off of my original project OpenDirectoryBot

Feel free to use this bot for your own use but be sure to check back often and watch https://wasson.io/redditsucker for new features and (hopefully in the future) Bot to Bot peering. This bot is not intended to distribute copyrighted material.

#Getting Started
Get the source code with "git clone https://bitbucket.org/chpwssn/redditsucker.git"

Enter the redditsucker directory

Run the install.sh script

Copy the example-config.py to config.py

Edit the config.py 

Run the script with "python redditsucker.py" or set it up as a cron job

#Updating Your Bot/Script
Enter your redditsucker directory

Execute a "git pull", this should not overwrite your config.py

#ToDo List
An opt-in web tracker to keep track of your downloads? Possibly with an API?

Ability to organize downloads via subreddit name (pics/, opendirectories/, etc.)

